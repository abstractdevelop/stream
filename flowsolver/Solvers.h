#include <RecFcn.h>
#include <vector>

enum Normals {corner, plusx, minusx, plusy, minusy, interior};

class NavierStokesSolver {
  IoData ioData;

  std::vector<double> local_timestep;
  double dt;
  int nPts, nCells, nFaces, nEdges; // domain

  int length;
  int width;
  int height;

  double alpha;
  double fx;
  double fy;
  double fz;

  std::vector<double> dx;
  double dy;
  double dz;

  double timestep;
  double cfl;

  State state;
  Flux flux;
  Reconstruction reconstruction;
  std::vector<Output_variable> outVar;   

  double gamma;

  std::string restartFile;
  std::string probesFile;
  int probeNode;
  int frequency;
 
  std::vector<double> V[5]; // vector of states: 0, u; 1, v; 2, w; 3, rho; 4, p
  std::vector<double> Vup[5];
  RecFcn recfcn;

  int wallDisp[3];
  int nInternalPts;
  std::vector<double> internalNode;
  std::vector<int> nodeId;
  std::vector<Normals> normal;

  std::vector<double> xfluxFcn[5]; // flux vector in x direction
  std::vector<double> yfluxFcn[5]; // flux vector in y direction
  std::vector<double> zfluxFcn[5]; // flux vector in z direction

  double fluxTime;  
  double IntegrationTime;
  double ioTime;
public:
  NavierStokesSolver(IoData ioDat);
  ~NavierStokesSolver();

public:

  void setupOutputFiles();
  int writeOutput(double time, int iteration);
  void readInputFile(std::string argv0);
  void readEmbeddedNodes(std::string argv0);

  // time integration
  void solve();
  void singleTimeIntegration();
  double computeMaxTimeStep();

  // 3d flux computation
  void computeEdgeAlignedFluxFcn(RecFcn RecFcn, std::vector<double> (&fluxfcn)[5], int n);
  void computeEulerFluxes();

  void computeBodyNormals();

  // XXX repeated code!!
  void computeDerivativesX(double& du, double& dv, double& dw, double& drho, double& dp, int i, int x); 
  void computeDerivativesY(double& du, double& dv, double& dw, double& drho, double& dp, int i, int y);
  void computeDerivativesZ(double& du, double& dv, double& dw, double& drho, double& dp, int i, int z);

  int restartOutput(double time, int maxIts);
  void probeOutput(double time, int iteration);
};

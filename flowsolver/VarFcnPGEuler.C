#include <cmath>
using namespace std;

#include "VarFcn.h"

/* This file is for the perfect gas equation of state
   The PG EOS is:
   p = (gamma - 1) * rho * e,
   where
   - gamma is the specific heat ratio
   - p is the pressure
   - rho is the density
   - e is the total internal energy */

// compute speed of sound
// sound = (gamma * p) / rho
double computeSoundSpeed(double p, double rho, double gamma)
{
  return sqrt((gamma * p) / rho);
}

//---------------------------------------------------------------------------

// compute mach number
// mach = (sqrt(abs(u^2 + v^2 + w^2))) / sound,
// where u is the x-axis velocity component, v the y-axis velocity component, and w the z-axis velocity component
double computeMachNom(double u, double v, double w, double p, double rho, double gamma)
{
  return sqrt(fabs(u*u + v*v + w*w)) / computeSoundSpeed(p, rho, gamma);
}

//---------------------------------------------------------------------------

// compute the total internal energy
// e = (1 / (gamma - 1)) * (p / rho)
double computeSpecificInternalEnergy(double p, double rho, double gamma)
{
  return (1 / (gamma - 1)) * (p / rho);
}

//---------------------------------------------------------------------------

double computeTotalInternalEnergy(double p, double rho, double u, double v, double w, double gamma)
{
  return rho*(computeSpecificInternalEnergy(p, rho, gamma) - 0.5 * (u * u + v * v + w * w));
}

template<typename T>
class Hessian {
private:
  // n = -1, m = 0, k = +1
  double dx2;  

public:
  Hessian(double dx):dx2(dx*dx) {};

  ~Hessian() {};

  // call operator
  T operator()(double uk, double um, double un)
  {
    // centered difference:
    // (un+1 - 2 * un + un-1) / (dx ^ 2)
    return (uk - 2 * um + un) / (dx2);
  }
};

class RecFcn {
private:

  int nPts;
  int length;
  int width;
  int height;

  // left states
  double uL;
  double pL;
  double rhoL;

  // right states
  double uR;
  double pR;
  double rhoR;

public:

  std::vector<double> recFcnL[5];
  std::vector<double> recFcnR[5];
 
  RecFcn() {}; // default constructor
  RecFcn(int x, int y, int z);
  ~RecFcn();

  void computeConstantRecFcn(std::vector<double> V[5], int n);

  // linear rec.
  void computeLinearRecFcn(std::vector<double> V[5], int n);
  double computeAverage(double a, double b);
  void computeConstStates(int i, int d, double Vl, double Vr);
  void simpleLimiter(double& alin, double acon, double restrict);
  void computeWafStates(int i, int d, double Vl2, double Vl, double Vr, double Vr2);
};

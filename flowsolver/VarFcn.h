#ifndef VARFCN
#define VARFCN

double computeSoundSpeed(double p, double rho, double gamma); // c
double computeMachNom(double u, double v, double w, double p, double rho, double gamma); // mach
double computeSpecificInternalEnergy(double p, double rho, double gamma); // e
double computeTotalInternalEnergy(double p, double rho, double u, double v, double w, double gamma); // E

#endif

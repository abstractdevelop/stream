/* This class is for performing runga-kutta integrations of
   the first, second, and fourth orders on an ordinary differential
   equation (ODE). In this case, the ODE is the euler equations discretized
   in space.
   Made by Miles F. Avery, 2018 */

class RKIntegrator
{
  private:
    double timestep; // the timestep

  public:
    RKIntegrator() {}; // default constructor
    RKIntegrator(double dt);
    double integrate(double un, double flux); // first order
    double integrate(double un, double flux, double fluxn); // second order, overload 
};

#include <iostream>
#include <cstring>
using namespace std;

#include <omp.h>

#include "IoData.h"
#include "Solvers.h"


int main(int argc, char** argv)
{
  IoData ioData(argv, argc); // input data
  cout << "running Stream...\n";
  ioData.initializeVariables();
  ioData.readInputs();

  NavierStokesSolver solver(ioData);
  solver.readInputFile(argv[2]);
  if (ioData.wallData.fitted_surface == true) {
    solver.readEmbeddedNodes(argv[3]); // read file
  }

  solver.solve();
}

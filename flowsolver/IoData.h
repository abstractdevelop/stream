#ifndef IODATA
#define IODATA


#include <string>
#include <vector>

enum Flux {godu, hlle, hllc, roe};
enum Wall_bc {slip, stick};
enum Reconstruction {constant, linear};
enum Equation {navierstokes, euler};
enum Mass_model {compressible, incompressible};
enum Turbulence_model {laminar, iles};
enum State {exact, acoustic, pvrs, two_rarefaction, two_shock};
enum Output_variable {velocity, density, pressure, mach, temperature, soundspeed, entropy, none};

//------------------------------------------------------------------

struct OutputData {
  int frequency;
  int max_outvars;
  std::vector<Output_variable> output_variable;

  int probe;

  std::string restart_file;
  std::string probe_file;
};

//------------------------------------------------------------------

struct SchemeData {
  Flux flux;
  Reconstruction reconstruction;
  State state;

  // for the exact solver
  double minres = 1e-5;
  double maxits = 100;
};

//---------------------------------------------------------------

struct EquationData {
  double reynolds;
  Equation equation;
  Turbulence_model turbulence_model;
  Mass_model mass_model;
};

//------------------------------------------------------------------

struct DomainData {
  double dx;
  double dy;
  double dz;

  double length;
  double width;
  double height;

  double numPts;
  
  bool streched;
  double dx_min;
  double dx_max;
};

//-----------------------------------------------------------------

struct ProblemData {
  double gas_constant;
  double specific_heat_ratio;
  int max_its;
  double final_time;

  double alpha;
  double force[3];
};

//----------------------------------------------------------------------

struct WallData {
  bool extrapdensity;
  double wall_temp;
  bool adiabatic = false;
  bool fitted_surface;
  int wallDisplacement[3];
  int frequency;
  Wall_bc wall_bc;
};

//----------------------------------------------------------------

struct ExplicitData {
  bool local_timestepping;
  bool compute_critical_timestep;
  double timestep;
  double cfl;
  
};

//-------------------------------------------------------------------

class IoData {
private:
  std::vector<std::string> argv0;
  std::string fnamein;
  int argc0;
  
public:
  IoData(char **argv, int argc);
  void initializeVariables();
  void readInputs();
  void comment(std::ifstream& fin);
  void c_style_comment(std::ifstream& fin);

public:
  EquationData equationData;
  SchemeData schemeData;
  WallData wallData;
  ProblemData problemData;
  OutputData outputData;
  DomainData domainData;
  ExplicitData explicitData;
};

#endif

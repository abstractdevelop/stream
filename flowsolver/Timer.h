double getTime(void);

struct Timer {
  double start_time;
  double end_time;
  double total_time;
};


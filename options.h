// this file contains the compile time options for cfd3d

constexpr bool on = true;
constexpr bool off = false;

constexpr bool checksolution = on; // check for negative pressure's and density's
constexpr bool clip = off; // clip pressures and densities if negative
constexpr bool redofailedtimestep = off; // redo a timestep with smaller cfl if it fails
constexpr bool printerr = on; // if some error is detected, print a warning
constexpr bool debug = off; // print some extra standard output for debugging purposes

/* if (fabs(stateL - stateR) <= tol) 
   use the approximate solver
   else 
   use the exact solver */
constexpr bool clipflux = on;
constexpr double tol = 0.001;
constexpr bool rarefact_left = off;

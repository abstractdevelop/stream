#include <cmath>
#include <fstream>
#include <iostream>



double f(int x)
{
  int rad = 10;
  return std::sqrt(std::abs(rad*rad - x*x));

  /*  int t = 5;
  int c = 10;
  return t * (1.4845 * std::sqrt(x/c) - 0.6300 * (x/c) - 1.7580 * (x/c) * (x/c) + 1.4215 * (x/c) * (x/c) * (x/c) - 
              0.5075 * (x/c) * (x/c) * (x/c) * (x/c)); */
}

int main()
{
  int rad = 10;
  int x0 = (200 - rad) * 0.5;
  int y0 = (100 - rad) * 0.5;

  std::ofstream fout;
  fout.open("hexsurf.txt");

  int x = x0;
  int y = y0;

  for (int k = 0; k < 2; ++k) {
    int j = 0;
    for (j = 0; j < rad; ++j) {
      double yd = f(j);
      int ym;
      ym = std::ceil(yd);

      for (int i = -ym+0.5*100; i < ym+0.5*100; ++i) {
        fout << j+0.5*200 << ' ' << i << ' ' << k << std::endl;
        fout << 0.5*200-j << ' ' << i << ' ' << k << std::endl;
      }
    }
  }
}

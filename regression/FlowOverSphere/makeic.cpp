#include <cstdlib>
#include <fstream>
#include <iostream>

int main(int argc, char**argv)
{
  std::ofstream fout;
  fout.open("hex.txt");

  int length = 200;
  int width = 100;
  int height = 2;
  int nPts = length * width * height;

  for (int z = 0; z < height; ++z) {
    for (int y = 0; y < width; ++y) {
      for (int x = 0; x < length; ++x) {
          fout << 0.1*34600 << ' '
               << 0 << ' '
               << 0 << ' '
               << 1.225e-3 << ' '
               << 1e6 << std::endl;

      }
    }
  }

  fout.close();
}

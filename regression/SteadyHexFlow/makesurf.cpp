#include <fstream>

int main()
{
  int blength = 40;
  int bwidth  = 10;
  int x0 = (200 - blength) * 0.5;
  int y0 = (100 - bwidth) * 0.5;

  std::ofstream fout;
  fout.open("hexsurf.txt");

  int x = x0;
  int y = y0;

  fout << blength * bwidth * 2 << std::endl;

  for (int k = 0; k < 2; ++k) {
    for (int j = 0; j < bwidth; ++j) {
      for (int i = 0; i < blength; ++i) {
        fout << x << ' ' << y << ' ' << k << std::endl;
        ++x;
      }
      ++y;
      x = x0;
    }

    x = x0;
    y = y0;
  }
}

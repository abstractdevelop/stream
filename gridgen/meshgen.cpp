#include <string>
#include <stdio.h>
#include <iostream>
using namespace std;

#include "meshgen.h"

MeshGen::MeshGen() { };

//--------------------------------------------------

void MeshGen::setdomain(int l, int w, int h, DomainData dim)
{
  x = l;
  y = w;
  z = h;
  domain = dim;
}

//--------------------------------------------------------------------
// Iodin: good name for character in Redmans Return

void MeshGen::writeMesh(string fname)
{
  printf("... Meshing fluid domain...\n");
  printf("... Writing %d nodes and %d elems to %s\n", x*y*z, (x-1)*(y-1)*(z-1), fname.c_str());

  int elemId = 117;
  FILE *fout = fopen(fname.c_str(), "w"); // open xpost file
  fprintf(fout, "Nodes FluidNodes\n");

  int n = 0;
  for (int k = 0; k < z; ++k)
    for (int j = 0; j < y; ++j)
      for (int i = 0; i < x; ++i) {
        n++;
        double dx = domain.dx;
        fprintf(fout, "%d %f %f %f\n", n, i * dx, j * domain.dy, k * domain.dz);
      }

  fprintf(fout, "Elements FluidElems using FluidNodes\n");
   
  n = 0;
  for (int m = 1; m <= (x - 1) * (y - 1) * (z - 1); ++m) {
    n += ((n + 1) % x == 0) ? 2 : 1;
    fprintf(fout, "%d %d %d %d %d %d %d %d %d %d\n", m, elemId, n, n + 1, n + x + 1, n + x, n + x*y, n + 1 + x*y, n + x + 1 + x*y, n + x + x*y);
  }

  fclose(fout);

}


#include <IoData.h>
#include <string>

class MeshGen {
private:
  DomainData domain;

  int x;
  int y;
  int z;

  int dx = 1;
  int dy = 1;
  int dz = 1;

public:
 
  MeshGen(); 
  void setdomain(int l, int w, int h, DomainData dim);
  void writeMesh(std::string fname);
};

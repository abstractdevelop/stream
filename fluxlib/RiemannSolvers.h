#include <IoData.h>

// external function used for evaluating the fluxfcn of arbitrary states
double computeFluxFcn(int i, double u, double v, double w, double p, double rho, double gamma);

/* This class is used for evaluating the numerical flux function.
   It includes the gudonov, hlle, and hllc Riemann solvers, as well as a simplified roe. */


class FluxFcn {
  State state; // the approximate state
  int maxgodits;
  double mingodres; 

  double gamma;
  double gam1;
  double gam2;
  double gam12;
  double invgam1;

public:
  // the fluxes
  double flux0;
  double flux1;
  double flux2;
  double flux3;
  double flux4;
  
  // the states
  double uL = 0, uR = 0;
  double vL = 0, vR = 0;
  double wL = 0, wR = 0;
  double pL = 0, pR = 0;
  double rhoL = 0, rhoR = 0;

  FluxFcn(double _gamma, int _maxgodits, double _mingodres, State _state);

  double computeFluxFcnL(int i);
  double computeFluxFcnR(int i);

  void computeStarValues(double& pstar, double &ustar);
 

  // HLLE and HLLC related functions
  void computeHLLEWaveSpeeds(double& aL, double& aR, double& p, double& u);
  void computeHLLE_Fluxes();
  void computeHLLC_Fluxes();

  // Godunov functions
  void computeGODU_Fluxes();
  void computeROE_Fluxes();

};

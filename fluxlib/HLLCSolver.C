#include "RiemannSolvers.h"

void FluxFcn::computeHLLC_Fluxes()
// as documented in Toro's book Riemann Solvers and Numerical Methods for Fluid Dynamics
// Article 10.4
{
  double pstar, ustar, aL, aR;
  computeHLLEWaveSpeeds(aL, aR, pstar, ustar);

  // there are two ways to compute astar, but the method #1 out has some special properties (see Toro's book)

  // method #1
  double astar = (pR - pL + rhoL * uL * (aL - uL) - rhoR * uR * (aR - uR)) / (rhoL * (aL - uL) - rhoR * (aR - uR));

  // method #2: unused currently
  // double astar = ustar;

  // warning! this code is shared by the hlle solver
  if (aL > 0 && aR > 0) {
    flux0 = computeFluxFcnL(0);
    flux1 = computeFluxFcnL(1);
    flux2 = computeFluxFcnL(2);
    flux3 = computeFluxFcnL(3);
    flux4 = computeFluxFcnL(4);

  } else if (aR < 0 && aL < 0) {
    flux0 = computeFluxFcnR(0);
    flux1 = computeFluxFcnR(1);
    flux2 = computeFluxFcnR(2);
    flux3 = computeFluxFcnR(3);
    flux4 = computeFluxFcnR(4);

  } else {
    // XXX somethings wrong here!
    if (aL < 0 && 0 < astar) {

      double F0L = computeFluxFcnL(0);
      double F1L = computeFluxFcnL(1);
      double F2L = computeFluxFcnL(2);
      double F3L = computeFluxFcnL(3);
      double F4L = computeFluxFcnL(4);

      double ustarL    = rhoL * ((aL - uL) / (aL - astar)) * astar;
      double vstarL    = rhoL * ((aL - vL) / (aL - astar)) * vL;
      double wstarL    = rhoL * ((aL - wL) / (aL - astar)) * wL;
      double rhostarL  = rhoL * ((aL - rhoL) / (aL - astar));
      double pstarL    = rhoL * ((aL - pL) / (aL - astar)) * (((pL / (rhoL * gam1)) / rhoL) + (astar - uL) * (astar + (pL / (rhoL * (aL - uL)))));

      flux0 = F0L + aL * (rhostarL - rhoL);
      flux1 = F1L + aL * (ustarL - uL);
      flux2 = F2L + aL * (vstarL - vL);
      flux3 = F3L + aL * (wstarL - wL);
      flux4 = F4L + aL * (pstarL - pL);
    } else {

      double F0R = computeFluxFcnR(0);
      double F1R = computeFluxFcnR(1);
      double F2R = computeFluxFcnR(2);
      double F3R = computeFluxFcnR(3);
      double F4R = computeFluxFcnR(4);

      double ustarR    = rhoR * ((aR - uR) / (aR - astar)) * astar;
      double vstarR    = rhoR * ((aR - vR) / (aR - astar)) * vR;
      double wstarR    = rhoR * ((aR - wR) / (aR - astar)) * wR;
      double rhostarR  = rhoR * ((aR - rhoR) / (aR - astar));
      double pstarR    = rhoR * ((aR - pR) / (aR - astar)) * (((pR / (rhoR * gam1)) / rhoR) + (astar - uR) * (astar + (pR / (rhoR * (aR - uR)))));

      flux0 = F0R + aR * (rhostarR - rhoR);
      flux1 = F1R + aR * (ustarR - uR);
      flux2 = F2R + aR * (vstarR - vR);
      flux3 = F3R + aR * (wstarR - wR);
      flux4 = F4R + aR * (pstarR - pR);
    }
  }
}

